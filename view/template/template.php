<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <?php
			$datapreference = helper::getClient();
			$dttitle = '';
			$dtemail = '';
			$dtphone = '';
			if(isset($datapreference['name']))
				$dttitle = $datapreference['name'];
			$datapreference = helper::getPreferences();
			if(isset($datapreference['email']))
				$dtemail = $datapreference['email'];
			if(isset($datapreference['phone']))
				$dtphone = $datapreference['phone'];
			
		?>
        <title><?php echo $dttitle; ?></title>
         <?php
        	if(isset($css)){
        		foreach ($css as $sc) {
					echo $sc;
				}
        	}
        ?>
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-xs-5 col-sm-5 col-md-5">
                        <div class="logo">
                        <?php
								$db = Db::init();
								$preference = helper::getPreferences();	
								echo '<a href="/welcome/index" title="'.$preference['name'].'"><img src="/showfile/show?namafile='.$preference['logo'].'&w=300&h=75" alt=""></a>';	
						?>
                        </div>
                    </div>
                    <div class="cart-and-search pull-right">
                        <div class="col-xs-1 col-sm-1 col-md-1">
                            <div class="side-menu-icon">
                                <ul>
                                    <li class="sb-open-left"><a id="right-menu" href="#right-menu"><i class="fa fa-bars"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- End header -->
        <!-- Start sidr menu -->
        <div id="sidr-right" class="sidr right" style="display: none;">
            <div class="side-menu">
                <nav>
                    <ul>
                        <li><a href="/welcome/index">Homepage</a></li>
                        <?php 
							foreach ($dmenu as $d){
							$name = "";
							foreach($d['detail'] as $dd ){
								if ($dd['language_id'] == $_SESSION['language']){
									$name = $dd['name'];
									break;	
								}
								
							}
							echo '<li class="dropdown">
										<a href="/content/index?id='.$d['_id'].'">'.$name.'</a>
								  </li>';
							}
						?>
                        <li><a href="/contact/index">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- End sidr menu -->
        <!-- Start slider -->
        
        <!-- End slider -->
        <!-- Start main menu -->
        <div class="main-menu">
            <div class="container">
                <div class="row">
                    <div class="menu">
                        <nav>
                            <ul>
                                <li><a href="/welcome/index">Homepage</a></li>
                                <?php 
									foreach ($dmenu as $d){
									$name = "";
									foreach($d['detail'] as $dd ){
										if ($dd['language_id'] == $_SESSION['language']){
											$name = $dd['name'];
											break;	
										}
									}
									echo '<li class="dropdown">
												<a href="/content/index?id='.$d['_id'].'">'.$name.'</a>
										  </li>';
									}
								?>
                                <li><a href="/contact/index">Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <?php echo $content; ?>
        <!-- End main menu -->
        <!-- Start features area -->
        
        <!-- End features area -->
        <!-- Start design shop area -->
        
        <!-- End design shop area -->
        <!-- Start count area -->
        
        <!-- End count area -->
        <!-- Start team member area -->
        
        <!-- End team member area -->
        <!-- Start portfolio area -->
        
        <!-- End portfolio area -->
        <!-- Start optimize area -->
        
        <!-- End optimize area -->
        <!-- Start blog area -->
        
        <!-- End blog area -->
        <!-- Start client area -->
       
        <!-- End client area -->
        <!-- Start footer -->
        <footer>
            <div class="footer-copy-right">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="copyright-text">
                                <p>Copyright © 2016 <a href="#">Kakiatna Energi</a></p>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="footer-menu">
                                <nav>
                                    <ul>
                                        <li><a href="#">FACEBOOK</a></li>
                                        <li><a href="#">GOOGLE PLUS</a></li>
                                        <li><a href="#">TWITTER</a></li>
                                        <li><a href="#">SERVICES</a></li>
                                        <li><a href="#">LINKEDIN</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- End footer -->
        
        <!-- JS -->
		<?php
			if(isset($js))
			{
				foreach($js as $scr)
				{	
					echo '<script type="text/javascript" src="'.$scr.'"></script>';	
				}
			}
		?>
 		
        <script>
        /*========================= 
         stickyNavTop
        ===========================*/ 
          var stickyNavTop = $('.main-menu').offset().top;
           
          var stickyNav = function(){
          var scrollTop = $(window).scrollTop();
                
          if (scrollTop > stickyNavTop) { 
              $('.main-menu').addClass('sticky');
          } else {
              $('.main-menu').removeClass('sticky'); 
          }
          };

          stickyNav();
           
          $(window).scroll(function() {
              stickyNav();
          }); 
        </script>
    </body>
</html>
