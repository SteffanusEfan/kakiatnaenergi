<div class="slider">
            <!-- Start Slider -->
        <div id="slider">
            <div class="fullwidthbanner1 tp-banner-container">
                <div class="fullwidthbanner-1 tp-banner" >
                    <ul>    <!-- SLIDE 1 -->
                        
                            <!-- MAIN IMAGE -->
                            <?php
                            	$db = Db::init();
								$col = $db->slider;
								$img = $col->find(array('client_id' => CLIENTID))->sort(array('time_created'=>1));
								
								foreach ($img as $val) {
									echo '
								
                            <li data-transition="fade" data-slotamount="7" data-masterspeed="1500" >
                            <img src="/showfile/show?namafile='.$val['filename'].'&w=1920&h=850"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
                            <!-- LAYERS -->
                            <!-- LAYER NR. 1 -->
                            <div class="tp-caption lfl"
                                data-x="-400"
                                data-y="center" data-voffset="0"
                                data-speed="1200"
                                data-start="500"
                                data-easing="easeInOutExpo"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                data-endspeed="300"
                                style="z-index: 5;"><img src="/public/img/slider/01.png" alt="">
                            </div>
                            <!-- LAYER NR. 2 -->
                            <div class="tp-caption lfr"
                                data-x="right" data-hoffset="-233"
                                data-y="center" data-voffset="220"
                                data-speed="1200"
                                data-start="1000"
                                data-easing="easeInOutExpo"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                 data-endspeed="300"
                                style="z-index: 6;"><img src="/public/img/slider/2.png" alt="">
                            </div>
                            <!-- LAYER NR. 3 -->
                            <div class="tp-caption largeHeading customin ltl tp-resizeme"
                                data-x="35"
                                data-y="center" data-voffset="-120"
                                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                data-speed="800"
                                data-start="2200"
                                data-easing="Back.easeInOut"
                                data-splitin="chars"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                 data-endspeed="400"
                                data-endeasing="Back.easeIn"
                                style="z-index: 7; max-width: ; max-height: ;color:#fff; font-size:65px; font-weight: bold; white-space: normal; margin-bottom: 0;"><h1></h1>
                            </div>
                            <!-- LAYER NR. 4 -->
                            <div class="tp-caption largeHeading global-link-color customin ltl tp-resizeme"
                                data-x="35"
                                data-y="center" data-voffset="-35"
                                data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                data-speed="800"
                                data-start="2950"
                                data-easing="Back.easeInOut"
                                data-splitin="chars"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                 data-endspeed="400"
                                data-endeasing="Back.easeIn"
                                style="z-index: 8; color:#ff2b42; font-size:60px; font-weight: bold; max-width: auto; max-height: auto; white-space: normal;"><h1>'.$val['title'].'</h1>
                            </div>
                            <!-- LAYER NR. 5 -->
                            <div class="tp-caption detailText p sfr ltl tp-resizeme hidden-xs"
                                data-x="35"
                                data-y="center" data-voffset="45"
                                data-speed="800"
                                data-start="3550"
                                data-easing="easeInOutExpo"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                 data-endspeed="400"
                                data-endeasing="Back.easeIn"
                                style="z-index: 9; max-width: auto; max-height: auto; white-space: nowrap; color:#fff;"><p>'.$val['description'].'</p>
                            </div>
                            <!-- LAYER NR. 6 -->
                            <div class="tp-caption sfr ltl tp-resizeme"
                                data-x="35"
                                data-y="center" data-voffset="135"
                                data-speed="800"
                                data-start="4000"
                                data-easing="easeInOutExpo"
                                data-splitin="none"
                                data-splitout="none"
                                data-elementdelay="0.1"
                                data-endelementdelay="0.1"
                                 data-endspeed="400"
                                data-endeasing="Back.easeIn"
                                style="z-index: 10; max-width: auto; max-height: auto; white-space: nowrap; border-width: 2px;font-size: 11px;letter-spacing: 0;line-height: 13px;margin: 0 0 4.09091px;min-height: 0;min-width: 65.4545px;padding: 12px 53px 12px 16px; color:#fff;">
                            </div>
                        </li>';
                        	}
                        ?>
                        <!-- SLIDE 2 -->
                        
                    </ul>
                    <div class="tp-bannertimer"></div>
                </div>
            </div>
        </div>
        <!-- End Slider -->
        </div>

<div class="features-area">
        <div class="container">
            <div class="row">
                <div class="featuer-top text-center">
                    <?php
						$db = Db::init();
						$preference = helper::getPreferences();
						$intitle = array();
						$intro = "";
						foreach ($preference['intro'] as $key) {
							$intitle = $key['intro_title'];
							$intro = $key['intro'];
						}	
							echo '<h2>'.$intitle.'</h2>';
							echo '<p>'.$intro.'</p>';
					 ?>
                </div>
                <div class="feature-box-area text-center">
                   
                        
                        <?php
							$db = Db::init();
							$pref = $db->content_image;
							$datas = $pref->find(array('client_id' => CLIENTID));
							
							foreach ($datas as $img) {
								echo '<div class="col-xs-12 col-sm-4 col-md-4">';
									echo '<div class="feature-box">'; 
									echo '<i><img src="/showfile/show?namafile='.$img['filename'].'&w=50&h=50"/></i>';
									echo '<h3>'.$img['title'].'</h3>';
									echo '<p>'.$img['description'].'</p>';
									echo '</div>';
								echo '</div>';
							}
						?>   
						
                </div>
            </div>
        </div>
</div>
