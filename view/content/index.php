<div id="particles-js">
    <div class="container">
        <div class="row">
			<div class="col-md-6">
				<div class="top-title-k">
					<?php echo '<h1>'.$namacat.'</h1>'; ?>
				</div>                  				
			</div>
			<div class="col-md-6">
				<div class="again-back">
					<a href="/welcome/index" class="readon">Back</a>	
				</div>
			</div>
        </div>
    </div>
</div>

<div class="about-intensy-area">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="about-intensy-text">
                	<?php
						foreach ($data as $key) {
							$d = "";
							$dd = "";
							foreach ($key['content'] as $isi) {
								$d = $isi['title'];
								$dd = $isi['content'];
							}
							echo '<h1 class="about-intensy-title"><strong>'.$d.'</strong></h1>';
							echo '<ul>
                        			<li>'.$dd.'</li>
                        		  </ul>';
						}
					?>  
                    <!--<a class="readon border large black" href="#">Learn More</a>-->
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    new WOW().init();
</script>