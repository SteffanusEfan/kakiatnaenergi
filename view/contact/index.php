<div id="particles-js">
    <div class="container">
        <div class="row">
			<div class="col-md-6">
				<div class="top-title-k">
					<h1>Contact Us</h1>
				</div>                  				
			</div>
			<div class="col-md-6">
				<div class="again-back">
					<a href="/welcome/index" class="readon">Back</a>	
				</div>
			</div>
        </div>
    </div>
</div>
<div class="contact-page-area main-contact">
    <div class="container">
        <div class="row">
            <!-- contact-map start -->
            <div class="col-lg-8 col-md-8 col-sm-8 contact-main">
                <div class="contact-mapm row">
                	<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3966.2803149864944!2d106.8068904594671!3d-6.2267243555081455!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sThe+Energy+Lt.+25+Suite+B+SCBD+Lot+11A+Jl.+Jend.+Sudirman+Kav.+52-53+Senayan+Kebayoran+Baru!5e0!3m2!1sen!2s!4v1458110737894" width="100%" height="375"></iframe>
                    <!--<div id="googleMap" style="width:100%;height:375px;"></div>-->
                </div>
            </div>
            <!-- contact-map end -->
            <div class="col-xs-12 col-sm-4 col-md-4">
                <div class="row">
                    <div class="company-location">
                        <div class="locatopn-inner">
                            <?php 
                            	foreach ($data as $key) {
									echo '<div class="location-box">
			                            <div class="company-loc-icon">
			                                <i class="list-img-icon li li-map-pin"></i>
			                            </div>
			                            <div class="company-loc-text">
			                                <h3>Company Location</h3>
			                                <p>'.$key['address'].'</p>
			                            </div>
			                        </div>';
			                        echo '<div class="location-box">
			                            <div class="company-loc-icon">
			                                <i class="list-img-icon li li-mail-envelope"></i>
			                            </div>
			                            <div class="company-loc-text">
			                                <h3>Official E-mail Address</h3>
			                                <p>'.$key['email'].'</p>
			                            </div>
			                        </div>';
			                        echo '<div class="location-box">
			                            <div class="company-loc-icon">
			                                <i class="list-img-icon li li-phone"></i>
			                            </div>
			                            <div class="company-loc-text">
			                                <h3>Phone Number</h3>
			                                <p>'.$key['phone'].'</p>
			                            </div>
			                        </div>';
		                        }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">             
            <div class="contact-main-area">
                <!-- contact-accordion start -->
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="contact-title">
                        <h2>CONTACT WITH US</h2>
                        <span>
                            <i class="fa fa-envelope-o"></i>
                        </span>
                    </div>
                </div>
                <!-- contact-accordion end --> 
                <div class="contact-form-aream">
                    <form action="/contact/simpanData" method="post">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="control-group">
                                <label class="control-label" for="inputname">Name</label>
                                <input type="text" id="name" name ="name" placeholder="Your Name" required>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputemail">Email</label>
                                <input type="email" id="email" name="email" placeholder="Email address" required>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="inputsubject">Subject</label>
                                <input type="text" id="subject" name="subject" placeholder="Message Subject" >
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="control-group">
                                <label class="control-label" for="message">Message</label>
                                <textarea name="message" id="message" placeholder="Your Message here" cols="30" rows="10" required ></textarea>
                            </div>
                            <div class="control-group">
                                <input type="submit" id="submit" value="send message" class="readon contact-border border" name="contact_us_submit">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>