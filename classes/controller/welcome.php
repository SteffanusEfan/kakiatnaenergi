<?php

class welcome_controller extends controller {
	
	public function index() {
		if(!isset($_SESSION['language']))
			$this->setSessionLanguage();
		
		$db = Db::init();
		$p = $db->preference;
		$q = array(
			'client_id' => CLIENTID
		);
		$col = $p->findone($q);
		
		$var = array(
			'data' => $col
		);
		
		$this->render('welcome', 'welcome/index.php', $var);
	}
}
