<?php

class content_controller extends controller{
	
	public function index(){
		$id = $_GET['id'];
		$db = Db::init();
		$category = $db->category;
		$colcat = $category->findOne(array('menu_id' => trim($id),'client_id' => CLIENTID,'detail.language_id' => $_SESSION['language']));
		$idcat = $colcat['_id'];
		
		$menu = $db->menu;
		$colmenu = $menu->findOne(array('_id' => new MongoId(trim($id)),'detail.language_id' => $_SESSION['language']));
		$namecat = "";
		
		
		foreach ($colmenu['detail'] as $key) {
			if ($key['language_id'] == $_SESSION['language']){
					$namecat = $key['name'];
					break;	
			}
		}
		
		$content = $db->content;
		$p = array(
			'category_id' => trim($idcat),
			'client_id' => CLIENTID,
			'content.language_id' => $_SESSION['language']	
		);
		
		$col = $content->find($p)->sort(array('time_created'=>1)) ;
		
	
		$var = array(
			'data' => $col,
			'idmenu' => $id,
			'namacat' => $namecat
		);
		
		$this->render('content', 'content/index.php', $var);
		//$view = $this->getView(DOCVIEW."content/index.php", $col);
		
	}
}
