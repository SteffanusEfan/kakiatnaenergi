<?php 
	class contact_controller extends controller{
		
		public function index(){
			//$id = $_GET['id'];
			$db = Db::init();
			$q = $db->preference;
			
			$p = array(
					'client_id' => CLIENTID
			);
			
			$col = $q->find($p);
						
			$var = array(
					'data' => $col
			);
			
			$this->render('contact','contact/index.php',$var);
		}
		
		public function simpanData(){
			if(!empty($_POST)){
				$dtname = "";
				if(isset($_POST['name'])){
					$dtname = trim($_POST['name']);
				}
				$dtemail = "";
				if(isset($_POST['email'])){
					$dtemail = trim($_POST['email']);
				}
				$dtphone = "";
				if(isset($_POST['phone'])){
					$dtphone = trim($_POST['phone']);
				}
				$dtsubject = "";
				if(isset($_POST['subject'])){
					$dtsubject = trim($_POST['subject']);
				}
				$dtmessage = "";
				if(isset($_POST['message'])){
					$dtmessage = trim($_POST['message']);
				}
				
				$db = Db::init();
				$q  = $db->message;
						
				$p  = array(
					'client_id' => CLIENTID,
					'name_user' => $dtname,
					'email_user' => $dtemail,
					'phone_user' => $dtphone,
					'subject_user' => $dtsubject,
					'message_user' => $dtmessage,
					'time_created' => time(),
					'status'	=> "new"
				);
				$isi = $q->insert($p);
				
				$this->redirect("/");
				exit;
				
			}
			else {
				$this->redirect("/contact/index");
			}	
			
		}
	}
?>