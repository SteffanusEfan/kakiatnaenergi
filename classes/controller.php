<?php

class controller
{
	var $css;
	var $js;
	var $playlist = array();
	var $dta;
	
	public function __construct()
	{
		$this->css = array();
		$this->js = array();						
	}
	
	protected function setSessionLanguage() {
		$db = Db::init();
		$p = $db->preference;
		$q = array(
			'client_id' => CLIENTID
		);
		$col = $p->findone($q);	
		if(isset($col['language_id'])) {
			if(strlen(trim($col['language_id'])) > 0)
				$_SESSION['language'] = $col['language_id'];
		}
	}
	
	protected function before()
	{
		
	}
	
	protected function getView($filename, $variable)
	{
		extract($variable);
		ob_start();
		(include $filename);
		$content = ob_get_contents();
		ob_end_clean ();
		return $content;
	}

	protected function getPage()
	{
	    $page = 1;
		if(isset($_GET['page']))
			$page = $_GET['page'];
		
		if(strlen(trim($page)) > 0)
		{
		    $page = intval($page);
		}
		else {
		    $page = 1;
		}
		return $page;
	}
	
	protected function getSort()
	{
	    $sort = "time_created";
		if(isset($_GET['sort']))
			$sort = $_GET['sort'];
		
		return $sort;
	}
	
	protected function getSortType()
	{
		$tipesort = 'ASC';
		if (isset($_GET['tipesort'])){
		
			if($_GET['tipesort'] == "ASC")
				$tipesort = 'ASC';
			else
				$tipesort = 'DESC';
		}
		
		return $tipesort;
	}
	
	protected function setSort($sort)
	{
		if(strlen($sort) < 1)
			$sort = 'time_created';
		
		$setsort = array($sort => -1);
		if (isset($_GET['tipesort']))
		{
			if($_GET['tipesort'] == "ASC")
				$setsort = array($sort => 1);
			else
				$setsort = array($sort => -1);
		}
		return $setsort;
	}
	
	protected function redirect($page)
	{
		header( 'Location: '.$page ) ;
	}
	
	protected function render($group, $view, $var)
	{		

		$css[] = '<link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300" rel="stylesheet" type="text/css">';
		$css[] = '<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet" type="text/css">';
		$css[] = '<link rel="stylesheet" href="/public/css/bootstrap.min.css">';
		$css[] = '<link rel="stylesheet" href="/public/rs-plugin/css/settings.css" media="screen" />';
		$css[] = '<link rel="stylesheet" href="/public/css/owl.carousel.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/owl.theme.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/owl.transitions.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/jquery.sidr.dark.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/font-awesome.min.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/linea.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/fancb/jquery.fancybox.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/animate.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/normalize.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/main.css">';
		$css[] = '<link rel="stylesheet" href="/public/style.css">';
		$css[] = '<link rel="stylesheet" href="/public/css/responsive.css">';
		
		$js[] = '/public/js/vendor/jquery-1.11.3.min.js';
		$js[] = '/public/js/bootstrap.min.js';
		$js[] = '/public/rs-plugin/js/jquery.themepunch.tools.min.js';
		$js[] = '/public/rs-plugin/js/jquery.themepunch.revolution.min.js';
		$js[] = '/public/rs-plugin/js/rs.home.js';
		$js[] = '/public/js/owl.carousel.min.js';
		$js[] = '/public/js/jquery.sidr.min.js';
		$js[] = '/public/js/jquery.fancybox.js';
		$js[] = '/public/js/jflickrfeed.min.js';
		$js[] = '/public/js/jquery.mixitup.min.js';
		$js[] = '/public/js/jquery.scrollUp.min.js';
		$js[] = '/public/js/counterup.min.js';
		$js[] = '/public/js/waypoints.min.js';
		$js[] = '/public/js/wow.js';
		$js[] = '/public/js/particles.js';
		$js[] = '/public/js/plugins.js';
		$js[] = '/public/js/main.js';

		
		if(isset($_SESSION['language'])) {
			if(strlen(trim($_SESSION['language'])) == 0)
				$this->setSessionLanguage();			
		}
		else
			$this->setSessionLanguage();
		//--------------Menu--------------
		$db = Db::init();
		$menu = $db->menu;
		$q = array(
				'client_id'=> CLIENTID
				
		);
		$dmenu = $menu->find($q);
		//------------end Menu------------		
		

		
		$m = explode("/", $view);
		$mm = explode(".", $m[1]);
		$method = $mm[0];
		
		$content = $this->getView(DOCVIEW.$view, $var);		
		$controller = $group;
		
		$css = array_merge($css, $this->css);
		$js = array_merge($js, $this->js);	
		
		//include(DOCVIEW."template/comingsoon.php");
		
        if(strpos($_SERVER['HTTP_HOST'], 'dev') !== false)
            include(DOCVIEW."template/template.php");
        else
        	include(DOCVIEW."template/comingsoon.php");

	}
}
?>